FROM debian:buster-slim

MAINTAINER Jeremy Volkening <jdvolkening@gmail.com>

# Install dependencies
WORKDIR /config
COPY extra/pioneers-15.5.dev.tar.gz pioneers.tar.gz

# procps provides 'ps' for nextflow
RUN apt-get update && apt-get install -y \
      gcc \
      intltool \
      itstool \
      libglib2.0 \
      libxml2-utils \
      make \
      pkg-config
RUN mkdir pioneers && tar -xf pioneers.tar.gz -C pioneers --strip-components 1 \
    && cd pioneers && ./configure && make && make install && cd .. \
    && rm -rf /config

# Create regular user
RUN useradd -m -U pioneers
USER pioneers
WORKDIR /home/pioneers/

# server port
EXPOSE 33321
# admin port
EXPOSE 33322

ENTRYPOINT [ \
    "/usr/local/bin/pioneers-server-console", \
    "--port", \
    "33321", \
    "--admin-port", \
    "33322", \
    "--admin-wait" \
]
